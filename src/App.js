import React, { Component } from "react";
import ReactGA from "react-ga";
import $ from "jquery";
import "./App.css";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import About from "./Components/About";
import Resume from "./Components/Resume";
import Contact from "./Components/Contact";
import Testimonials from "./Components/Testimonials";
import Portfolio from "./Components/Portfolio";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      foo: "bar",
      resumeData: {},
    };

    ReactGA.initialize("UA-110570651-1");
    ReactGA.pageview(window.location.pathname);
  }

  getResumeData() {
    $.ajax({
      url: "/resumeData.json",
      dataType: "json",
      cache: false,
      success: function (data) {
        this.setState({ resumeData: data });
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(err);
        alert(err);
      },
    });
  }

  headerData = {
    name: "Mohammad Chaliffilardhy Syaifuddin",
    occupation: "Software Engineer",
    description: "A simple guy with complicated minds",
    address: { city: "Malang" },
    social: [
      {
        name: "LinkedIn",
        url: "https://www.linkedin.com/in/chalippi/",
      },
      { name: "Instagram", url: "@chalippp" },
    ],
  };
  resumeData = {
    skillmessage: "Skillfull enough to delete your production database",
    education: [
      {
        school: "Home Sweet Home",
        degree: "Crybaby",
        graduated: "About 20 years ago",
        description:
          "Learned how to kick balls like Tsubasa.\nBest achievement : destroying my mother's pick fence",
      },
      {
        school: "School of life",
        degree: "Unrated",
        graduated: "Undefined",
        description: "Error 500",
      },
    ],
    work: [
      {
        company: "Home",
        title: "Transporter",
        years: "Every year",
        description: "Loves to drive, loves to burn some gas",
      },
      {
        company: "Land of Dawn",
        title: "Positive Mental Attitude Player",
        years: "Season4++",
        description: "Dunno why I still stuck on this job",
      },
    ],
    skills: [
      { name: "Support", level: "75%" },
      { name: "Silence", level: "100%" },
      { name: "Love", level: "0.1%" },
    ],
  };
  aboutData = {
    name: "Mohammad Chaliffilardhy Syaifuddin",
    image: "https://storage.googleapis.com/sinau_bucket/profile.jpg",
    bio: "Doing some coding stuff while searching for purpose of life",
    address: {
      street:
        "Street : Not gonna tell. But it's floodded when those clouds use Hydro Pump",
      city: "City : M-A-L-A-N-G, please don't make me repeat it",
      state: "State : Jawi Wetan",
      zip: "ZIP : Google it",
    },
    phone: "Phone : Ask my girlfriend",
    email: "Email : ardhymcs@gmail.com",
    resumedownload: "https://storage.googleapis.com/sinau_bucket/resume.pdf",
  };

  componentDidMount() {
    this.getResumeData();
  }

  render() {
    console.log(this.headerData);
    return (
      <div className="App">
        <Header data={this.headerData} />
        <About data={this.aboutData} />
        <Resume data={this.resumeData} />
        <Portfolio data={this.state.resumeData.portfolio} />
        <Testimonials data={this.state.resumeData.testimonials} />
        <Contact data={this.state.resumeData.main} />
        <Footer data={this.state.resumeData.main} />
      </div>
    );
  }
}

export default App;
